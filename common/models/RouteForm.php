<?php
namespace common\models;

use Yii;
use yii\base\Model;

/**
 * Route form
 */
class RouteForm extends Model
{
    public $address;
    public $office;
    public $tip;
    public $note;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['address'], 'required'],
        ];
    }

    public static function getItemsToUpdate(){
        $result = [];
        if(isset($_POST['RouteForm'])) {
            foreach ($_POST['RouteForm'] as $key => $v) {
                $result[$key] = new self;
            }
        } else {
            for($i = 1; $i<=2; $i++) {
                $result[$i] = new self;
            }
        }
        return $result;
    }

}
