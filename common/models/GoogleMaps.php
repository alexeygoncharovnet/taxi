<?php
namespace common\models;

class GoogleMaps
{
    public static function getDistance($origin, $destination)
    {
        $params = array(
            'origin'        => $origin,
            'destination'   => $destination,
            'sensor'        => 'true',
            'units'         => 'metric',//'imperial'
            'language'      => 'ru',
            'driving'       => 'driving'
        );

        $params_string = '';
        foreach($params as $var => $val){
            $params_string .= '&' . $var . '=' . urlencode($val);
        }

        $url = "http://maps.googleapis.com/maps/api/directions/json?".ltrim($params_string, '&');

        // Make our API request
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $return = curl_exec($curl);
        curl_close($curl);

        $directions = json_decode($return);

        return $directions;
    }


    public static function getAddress($input)
    {
        //https://maps.googleapis.com/maps/api/place/autocomplete/json?input=Vict&types=geocode&language=fr&sensor=true&key=API_KEY
        $url = 'https://maps.googleapis.com/maps/api/place/autocomplete/json?';

        if (strlen($input) > 0) {
            $params = array(
                'input'        => $input,
                'types'        => 'geocode',
                'language'      => 'ru',
                'sensor'       => 'true',
                'key'           => 'AIzaSyA_tcilzQpTNgKvVx4j7BlMJt8lkKEddvs',
                'components'    => 'country:ru'
            );

            $params_string = '';
            foreach($params as $var => $val){
                $params_string .= '&' . $var . '=' . urlencode($val);
            }
            $url = $url . ltrim($params_string, '&');

            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_REFERER, "http://event.dev");
            $return = curl_exec($curl);
            curl_close($curl);

            $result = json_decode($return);

            return $result;
        }
    }
}