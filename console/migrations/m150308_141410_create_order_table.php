<?php

use yii\db\Schema;
use yii\db\Migration;

class m150308_141410_create_order_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%order}}', [
            'id' => Schema::TYPE_PK,
            'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'payed' => Schema::TYPE_STRING . '(1) NOT NULL',
            'currency' => Schema::TYPE_STRING . '(3) NOT NULL',
            'tracking_number' => Schema::TYPE_STRING . '(32) NOT NULL',
            'phone' => Schema::TYPE_STRING . '(32) NOT NULL',
            'name' => Schema::TYPE_STRING . '(32) NOT NULL',
            'price' => Schema::TYPE_DECIMAL . '(18, 2) NOT NULL',
            'discount' => Schema::TYPE_DECIMAL . '(18, 2) NOT NULL',
            'comments' => Schema::TYPE_TEXT . ' NOT NULL',
            'status' => Schema::TYPE_STRING . ' NOT NULL',
            'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
            'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%order}}');
    }
}

