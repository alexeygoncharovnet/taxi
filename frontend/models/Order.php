<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "order".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $payed
 * @property string $currency
 * @property string $tracking_number
 * @property string $phone
 * @property string $name
 * @property string $price
 * @property string $discount
 * @property string $comments
 * @property string $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class Order extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'payed', 'currency', 'tracking_number', 'phone', 'name', 'price', 'discount', 'comments', 'status', 'created_at', 'updated_at'], 'required'],
            [['user_id', 'created_at', 'updated_at'], 'integer'],
            [['price', 'discount'], 'number'],
            [['comments'], 'string'],
            [['payed'], 'string', 'max' => 1],
            [['currency'], 'string', 'max' => 3],
            [['tracking_number', 'phone', 'name'], 'string', 'max' => 32],
            [['status'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'payed' => 'Payed',
            'currency' => 'Currency',
            'tracking_number' => 'Tracking Number',
            'phone' => 'Phone',
            'name' => 'Name',
            'price' => 'Price',
            'discount' => 'Discount',
            'comments' => 'Comments',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
