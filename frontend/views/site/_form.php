<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\captcha\Captcha;
use common\models\RouteForm;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model app\models\Order */
/* @var $form yii\widgets\ActiveForm */
?>

<?
$url = \yii\helpers\Url::to(['address-list']);

$initScript = <<< SCRIPT
function (element, callback) {
    var id=\$(element).val();
    if (id !== "") {
        \$.ajax("{$url}?id=" + id, {
            dataType: "json"
        }).done(function(data) { callback(data.results);});
    }
}
SCRIPT;

?>

<div class="row">

    <?php
    $form = ActiveForm::begin([
        'id' => 'login-form',
        'options' => ['class' => 'form-horizontal'],
        /*'fieldConfig' => [
            'template' => '{label}<div class="col-sm-10">{input}</div><div class="col-sm-10">{error}</div>',
            'labelOptions' => ['class' => 'col-sm-2 control-label'],
        ],*/
    ]);
    ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <? foreach($items as $i => $item) { ?>

        <?
        echo $form->field($item, "[$i]address")->widget(\kartik\select2\Select2::classname(), [
            'options' => ['placeholder' => 'Введите адресс ...'],
            'pluginOptions' => [
                'allowClear' => true,
                'minimumInputLength' => 3,
                'ajax' => [
                    'url' => $url,
                    'dataType' => 'json',
                    'data' => new JsExpression('function(term,page) { return {search:term}; }'),
                    'results' => new JsExpression('function(data,page) { return {results:data.results}; }'),
                ],
                'initSelection' => new JsExpression($initScript)
            ],
        ]);
        ?>

    <? } ?>

    <div class="btn-group" data-toggle="buttons">


        <?= $form->field($model, 'wifi', [
            'template' => '{label}<label class="btn btn-primary">{input}</label>',
        ])->checkbox(); ?>

    </div>

    <div class="form-group">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<label class="btn btn-primary">
    <input type="checkbox" autocomplete="off"> Checkbox 2
</label>
<label class="btn btn-primary">
    <input type="checkbox" autocomplete="off"> Checkbox 3
</label>