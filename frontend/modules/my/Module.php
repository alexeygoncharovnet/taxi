<?php

namespace app\modules\my;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\my\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
